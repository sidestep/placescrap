import {load, save, id, path, bookingGet, hotelUrl } from 'utils'

//extract pics from hotels (for colab)
function extractPics()
{
    const cc = 'nl'
    const hotels = load(path(cc, 'hotels'))
    const pics = hotels.map(([hid, {pics, rooms}]) => [[hid, pics], ...rooms.map(([rid, {pics}]) => [rid, pics])])
    save(`${cc}.json`, pics)
}
extractPics()

//merge tagged pic files (from colab)
//CC='it';echo "[$(cat ${CC}/*.json | sed 's/\]\[/]\,\n\[/g')]" > ${CC}.json #merge cc json

//const cc = 'ua'
//const hotels = load(path(cc, 'hotels'))
//const contacts = hotels.map(([hid, {name, adr}]) => [hid, `${name} - ${adr}`])
//save(`${cc}_contacts.json`, contacts)

//const cc = 'se'
//let pics = load(path(cc, 'pics'))
//pics = pics.map(([hpics, rpics]) => [hpics, ...rpics])
//save(path(cc, 'pics'), pics)
//Deno.exit(1)
//let ids = load(path(cc, 'ids'))
//ids = new Map(ids.map(i => [i.split(',')[0], i]))
//let tags = load(path(cc, 'tags'))
//tags = tags.filter(t => ids.has(id(t)))
//tags.forEach(t => t[0] = ids.get(t[0]))
//save(path(cc, 'tags'), tags)

//hotels.forEach(([_, h]) => h.loc = h.loc.replace(':', ','))
//save(path(cc, 'hotels'), hotels)
