import avifEncoder from './avif_enc.js';
//import { decode } from './jpegdecoder.js' 

const defaultOptions = {
    cqLevel: 33,
    cqAlphaLevel: -1,
    denoiseLevel: 0,
    tileColsLog2: 0,
    tileRowsLog2: 0,
    speed: 6,
    subsample: 1,
    chromaDeltaQ: false,
    sharpness: 0,
    tune: 0 /* AVIFTune.auto */,
};

const encoder = await avifEncoder({ noInitialRun: true, instantiateWasm: null})

export default function encode(imgData, options = {}) {
    const _options = { ...defaultOptions, ...options };
    const output = encoder.encode(imgData.data, imgData.width, imgData.height, _options);
    if (!output) {
        throw new Error('Encoding error.');
    }
    return output.buffer;
}

//export async function toAvif(src) 
//{
//      const rawImg = await fetch(src).then(res => res.bytes()).then(bytes => decode(bytes))
//      return encode(rawImg);
//}
//
//
//const raw = await toAvif('https://cf.bstatic.com/xdata/images/hotel/max640/571154265.jpg?k=6e8b839728d2418201bf149cbfe066b69d09989170dbeed3b9752e544c2c9514&o=&hp=1')
//
//Deno.writeFileSync('img', new Uint8Array(raw))
