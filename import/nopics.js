import {load, run, removeDuplicates, glob, mkdir } from 'utils'
import { loc2id, loc2crd } from 'wifi-bed/lib/coord.js'
import { specs, isError, toPlace, toHost} from 'wifi-bed/serv/mod/inputs.js'
import { Hosts, Places } from 'wifi-bed/serv/mod/repo.js'
import { europe, outBounds } from 'wifi-bed/serv/mod/regions.js'

mkdir('nopics')
const env = Deno.env
const dbScript = (name) => `${env.get('HOME')}/wifi-bed/infra/db/${name}.sh`
const dbPath = (name) => new URL(`nopics/${name}.db`, import.meta.url).pathname
const createDb = (name) => {run(`${dbScript(name)} ${dbPath(name)}`); return dbPath(name)}
const [placesDB, hostsDB] = [new Places(createDb('places')), new Hosts(createDb('hosts'))]

function offerVals(room)
{
    if(!room.rate)
        return {}
    try
    {
        const [minPax, minRate, maxPax, maxRate] = room.rate.split(',')
        if(!maxPax)
            return {pax: minPax, rate: Math.round(minRate)}
        if(minRate >= maxRate) // scenarios with weird price policy
           return {pax: maxPax, rate: minRate}
        return {pax: maxPax, rate: maxRate, minPax, minRate}
    }
    catch(e) 
    {
        console.error('bad room', e, room)
        throw e
    }
}
function savePlace(login, place)
{
    const pid = place.id
    if(placesDB.getId(pid))
    {
        const hid = hostsDB.id(login)                
        if(!hid)
            throw `place taken ${pid}`              
        placesDB.update(pid, hid, place)
        return                                       
    }
    const host = toHost(truncTxt({name: login}))
    if(isError(host))
        throw host.toString()
    place = toPlace(place)
    if(isError(place))
        throw place.toString()
    const hid = hostsDB.insert(login, host)
    placesDB.insert(pid, hid, cc, place)
}

const txtMax = {name: specs.name.range.maxlength, desc: specs.desc.range.maxlength}
const offersMax = specs.offers.range.maxlength
const truncTxt = (obj) => 
{
    for(const prop in txtMax) 
        if(prop in obj && obj[prop])
        {
            const [txt, maxLen] = [obj[prop], txtMax[prop]]
            if(txt.length > maxLen)
                obj[prop] = txt.substring(0, maxLen-3) + '...'
        }
    return obj
}

function toPlaces(hotels)
{
    const err = []
    const places = []
    for(const [hid, hotel] of hotels) 
    {
        try
        {
//            if(outBounds(loc2crd(hotel.loc), rgn))
//            {
//                err.push(`out bounds ${cc} ${hotel.loc}`)
//                continue
//            }
            hotel.rooms = hotel.rooms.map(([_,r]) => r)
            hotel.rooms = removeDuplicates(hotel.rooms)
            hotel.rooms = hotel.rooms.filter(r =>
            {
                if(!r.rate)
                {
                    err.push(`no rate ${hid} ${r.name}`)
                    return false
                }
                return true
            })
            if(hotel.rooms.length == 0)
            {
                err.push(`no import ${hid}`)
                continue
            }
            truncTxt(hotel)
            hotel.offers = hotel.rooms.slice(0, offersMax)
            hotel.offers.forEach(ofr => truncTxt(Object.assign(ofr, offerVals(ofr))))
            const place = toPlace(hotel)
            if(isError(place))
                err.push(`invalid ${hid} ${place.toString()}`)
            else
            {
                place.id = loc2id(hotel.loc)
                places.push([hid, place])
            }
        }
        catch(e)
        {
            console.error(e, hid)
        }
    }
    return [err, places]
}


let cc = undefined
let rgn = undefined
const hotelFiles = Array.from(glob('../data/**-hotels.json'))
console.info('[~] found', hotelFiles)
hotelFiles.forEach(f => 
    {

        cc = f.slice(-14).substring(0,2)
        console.info('[~] --- loading', cc, '---')
        rgn = europe[cc]
        const hotels = load(f)
        let [err, places] = toPlaces(hotels)

        const byId = Object.values(Object.groupBy(places, ([_,p]) => p.id))
        const dupCnt = byId.reduce((cnt, p) => cnt + p.length - 1, 0)
        places = byId.map(pl => pl[0])

        console.info('[~] duplicate loc:', dupCnt)
        console.info('[~] out bounds:', err.filter(e => e.startsWith("out bounds")).length)
        console.info('[~] no rate:', err.filter(e => e.startsWith("no rate")).length)
        console.info('[~] skipped:', err.filter(e => e.startsWith("no import")).length)
        console.info('[~] invalid:', err.filter(e => e.startsWith("invalid")).length)

        console.info(`[~] loaded ${places.length} ${cc} places out of ${hotels.length}`)
        console.info(`[~] importing ${places.length} places`)
        let errors = 0
        const t0 = Date.now();
        for (const [hid, place] of places)
        {
            try 
            {
                savePlace(hid, place)
            }
            catch(e)
            {
                console.log(e, hid);
                ++errors
            }
        }
        const t1 = Date.now();
        const time = (t1 - t0)/1000
        const total = places.length - errors 
        console.log(`[~] done in ${time}s. (${Math.round(total/time)} ops/s) ${total} imported, ${errors} errors`)
    })

placesDB[Symbol.dispose]()
hostsDB[Symbol.dispose]()
