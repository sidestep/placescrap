import { picUrl, run, sleep } from 'utils';
import { genUpload, pics as allPics } from 'wifi-bed/serv/store/api.js'
import { sign } from 'wifi-bed/serv/auth.js'

//const env = Deno.env
//const servUp = `${env.get('HOME')}/wifi-bed/serv/up.sh`
//env.set('PLACES_DB', dbPath('places'))
//env.set('HOSTS_DB', dbPath('hosts'))
//spawn(servUp)
//await sleep(1)

async function http(method, url, obj=null)
{
    const resp = await fetch(url, { method: method, body: obj ? JSON.stringify(obj) : null })
    const body = await resp.text()
    if(!resp.ok)
        throw body
    return body
}
//async function storePics(pid, place)
//{
//    const uploadPic = eval(genUpload(cc, pid))
//    for await(const [i, p] of place.pics.entries())
//        await uploadPic(`p${i}`, await jpg2avif(picUrl(id(p))))    
//    for (const [i, ofr] of place.offers.entries())
//        for await (const [j, p] of ofr.pics.entries())
//            await uploadPic(`${i}${j}`, await jpg2avif(picUrl(id(p)))) 
//}

async function savePlace(login, pid, place)
{
    const host = toHost(truncTxt({name: login}))
    if(isError(host))
        throw host.toString()
    const hid = await http('PUT', `http://localhost/wb/h?l=${login}`, host)
    await http('PUT', `http://localhost/wb/p?a=${sign(hid)}&h=${hid}&p=${pid}&r=${cc}`, place)
}

//const t0 = Date.now();
//const t1 = Date.now();
//const time = (t1 - t0)/1000
//console.log(`[~] done in ${time}s. ${Math.round(total * 2 / time)} ops/s`)


