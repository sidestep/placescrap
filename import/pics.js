import { id, picTag, tagScore } from 'utils'

const cmpTag = (p1,p2) => tagScore(p2) - tagScore(p1)

class ByTag
{
    constructor(pics)
    {
        pics = pics.filter(p => tagScore(p) >= 0.4)
        const tags = Object.groupBy(pics, picTag)
        Object.entries(tags).forEach(([k,v]) => tags[k] = v.sort(cmpTag))
        this.take = (tag) => tags[tag]?.shift()
    }
    get bedroom() { return this.take('bedroom with bed and furniture') || this.take('bedroom') }
    get bathroom() { return this.take('toilet with shower') || this.take('bathroom') }
    get kitchen() { return this.take('kitchen') }
    get livingroom() { return this.take('livingroom with furniture') }
    get outdoors() { return this.take('outdoors') }
    get house() { return this.take('house') }
    get reception() { return this.take('reception') }
}

function pickRoomPics({pics})
{
    const take = new ByTag(pics)
    const bedShot = take.bedroom 
    if(!bedShot) return []
    pics = [bedShot,
            take.kitchen || take.livingroom || take.bedroom,
            take.bathroom || take.house || take.outdoors ]
    return pics.filter(p => p)
}

function pickPlacePics(pics)
{
    const take = new ByTag(pics)
    pics = [take.kitchen, 
            take.livingroom || take.outdoors, 
            take.bathroom || take.outdoors || take.reception ]
    return pics.filter(p => p)
}

function splitPics(pics)
{
    const roomPics = pickRoomPics({pics})
    const takenPics = roomPics.map(id)
    const hotelPics = pics.filter(p => !takenPics.includes(id(p)))
    return [hotelPics, roomPics]
}

export function setPics(hotel)
{
    if(hotel.rooms.length == 1)
    { 
        const theRoom = hotel.rooms[0]
        const picPool = [hotel.pics, theRoom.pics].filter(p => p.length)
        if(picPool.length == 1) // then all pics are in one pool (like whole place offer). Single offer has all pics or hotel has all pics.
        {
            const [hotelPics, roomPics] = splitPics(picPool[0]) 
            theRoom.pics = roomPics
            hotel.pics = hotelPics
        }
    }
    const hotelPics = hotel.pics.filter(p => id(p) != hotel.img)
    const placePics = pickPlacePics(hotelPics)
    placePics.unshift([hotel.img, ['hotel', 1]])
    hotel.pics = placePics
    hotel.rooms.forEach(r => r.pics = pickRoomPics(r))
}
