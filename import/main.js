import { picUrl, loadAll, run, id } from 'utils';
import { decode } from './avif/jpegdecoder.js' 
import toAvif from './avif/main.js'
import { pics as allPics } from 'wifi-bed/serv/store/api.js'
import { loc2id, loc2crd } from 'wifi-bed/lib/coord.js'
import { specs, isError, toPlace, toHost} from 'wifi-bed/serv/inputs.js'
import { europe, outBounds } from 'wifi-bed/serv/regions.js'
import { Hosts, Places } from 'wifi-bed/serv/db/repo.js'
import { setPics } from './pics.js'

//if out of heap run: deno run -A --v8-flags=--max-old-space-size=8192 main.js
const cc = "se"
const rgn = europe[cc]

const env = Deno.env
const mkdir = (d) => Deno.mkdir(d, {recursive: true})
mkdir('data')
const picDir = (pid) => new URL(`data/${cc}/${pid}`, import.meta.url).pathname
const lsDir = (dir) => Array.from(Deno.readDirSync(dir)).map(f => f.name)
const rmDir = (dir) => Deno.removeSync(dir, {recursive: true})
const write = (p,f) => Deno.writeFileSync(p, f)
const dbScript = (name) => `${env.get('HOME')}/wifi-bed/infra/db/${name}.sh`
const dbPath = (name) => new URL(`data/${name}.db`, import.meta.url).pathname
const createDb = (name) => {run(`${dbScript(name)} ${dbPath(name)}`); return dbPath(name)}
const noDir = (p) => {try{return !(Deno.statSync(p)).isDirectory}catch(e){if(e instanceof Deno.errors.NotFound) return true; throw e;}}

function offerVals(room)
{
    if(!room.rate)
        return {}
    try
    {
        const [minPax, minRate, maxPax, maxRate] = room.rate.split(',')
        if(!maxPax)
            return {pax: minPax, rate: Math.round(minRate)}
        if(minRate >= maxRate) // scenarios with weird price policy
           return {pax: maxPax, rate: minRate}
        return {pax: maxPax, rate: maxRate, minPax, minRate}
    }
    catch(e) 
    {
        console.error('bad room', e, room)
        throw e
    }
}

const txtMax = {name: specs.name.range.maxlength, desc: specs.desc.range.maxlength}
const offersMax = specs.offers.range.maxlength
const truncTxt = (obj) => 
{
    for(const prop in txtMax) 
        if(prop in obj && obj[prop])
        {
            const [txt, maxLen] = [obj[prop], txtMax[prop]]
            if(txt.length > maxLen)
                obj[prop] = txt.substring(0, maxLen-3) + '...'
        }
    return obj
}

function toPlaces(hotels)
{
    const err = []
    const places = []
    for(const [hid, hotel] of hotels) 
    {
        try
        {
            if(outBounds(loc2crd(p.loc), rgn))
            {
                err.push(`out bounds ${cc} ${p.loc}`)
                continue
            }
            setPics(hotel)
            hotel.rooms = hotel.rooms.filter(r =>
            {
                if(!r.rate)
                {
                    err.push(`no rate ${hid} ${r.name}`)
                    return false
                }
                if(r.pics.length == 0)
                {
                    err.push(`no pics ${hid} ${r.name}`)
                    return false
                }
                return true
            })
            if(hotel.rooms.length == 0)
            {
                err.push(`no import ${hid}`)
                continue
            }
            truncTxt(hotel)
            hotel.offers = hotel.rooms.slice(0, offersMax)
            hotel.offers.forEach(ofr => truncTxt(Object.assign(ofr, offerVals(ofr))))
            const [placePics, roomPics] = [hotel.pics, hotel.rooms.map(r => r.pics)]
            const place = toPlace(hotel)
            if(isError(place))
                err.push(`invalid ${hid} ${place.toString()}`)
            else
            {
                place.id = loc2id(hotel.loc)
                place.pics = placePics
                place.offers.forEach((ofr,i) => ofr.pics = roomPics[i])
                places.push([hid, place])
            }
        }
        catch(e)
        {
            console.error(e, hid)
        }
    }
    return [err, places]
}

const jpg2avif = (src) => fetch(src)
    .then(res => 
    {
        if(!res.ok) throw `got ${res.status} for ${src}`; 
        return res.bytes()
    })
    .then(bytes => decode(bytes))
    .then(jpeg => new Uint8Array(toAvif(jpeg)))

function picUploads(place)
{
    const [ppics, rpics] = [allPics.slice(0,4), allPics.slice(4)]
    const roomPics = place.offers.map(o => o.pics).flat()
    return [place.pics.map((p,i) => [ppics[i], picUrl(id(p))]), 
        roomPics.map((p,i) => [rpics[i], picUrl(id(p))])].flat()
}
const filesDiffer = (dir, uploads) => lsDir(dir).sort().toString() != uploads.map(id).sort().toString()

function removeChangedPics(places)
{
    let cnt = 0
    places.forEach(([_, p]) =>
    {
        const dir = picDir(p.id)
        if(!noDir(dir) && filesDiffer(dir, picUploads(p)))
        {
            rmDir(dir)
            ++cnt
        }
    })
    return cnt
}
const writePics = (dir, uploads) => Promise.all(uploads.map(async ([name,url]) => write(`${dir}/${name}`, await jpg2avif(url))))
async function savePics(place)
{
    const dir = picDir(place.id)
    if(noDir(dir))
    {
        mkdir(dir)
        await writePics(dir, picUploads(place))
    }
}
function savePlace(login, place)
{
    const pid = place.id
    if(placesDB.getId(pid))
    {
        const hid = hostsDB.id(login)                
        if(!hid)
            throw `place taken ${pid}`              
        placesDB.update(pid, hid, place)
        return                                       
    }
    const host = toHost(truncTxt({name: login}))
    if(isError(host))
        throw host.toString()
    place = toPlace(place)
    if(isError(place))
        throw place.toString()
    const hid = hostsDB.insert(login, host)
    placesDB.insert(pid, hid, cc, place)
}

const hotels = loadAll(cc)
let [err, places] = toPlaces(hotels)
const byId = Object.values(Object.groupBy(places, ([_,p]) => p.id))
const dupCnt = byId.reduce((cnt, p) => cnt + p.length - 1, 0)
places = byId.map(pl => pl[0])
//places = removeClaimed(places)
console.info('[~] duplicate loc: ', dupCnt)
console.info('[~] no pics: ', err.filter(e => e.startsWith("no pics")).length)
console.info('[~] no rate: ', err.filter(e => e.startsWith("no rate")).length)
console.info('[~] skipped: ', err.filter(e => e.startsWith("no import")).length)
console.info('[~] invalid: ', err.filter(e => e.startsWith("invalid")).length)
console.log(err.filter(e => e.startsWith("invalid"))); //!--- delete me!

const changeCnt = removeChangedPics(places)

console.info(`[~] changed pics ${changeCnt}`)
console.info(`[~] loaded ${places.length} places out of ${hotels.length}`)
const utf8en = new TextEncoder()
let errors = 0
let cnt = 0
const [placesDB, hostsDB] = [new Places(createDb('places')), new Hosts(createDb('hosts'))]

console.info(`[~] importing ${places.length} places`)
for await(const [hid, place] of places)
{
    Deno.stdout.writeSync(utf8en.encode(`${cnt++}\r`))
    try 
    {
        await savePics(place)
        savePlace(hid, place)
    }
    catch(e)
    {
        console.log(hid, e);
        ++errors
    }
}

console.log(`[~] done: ${places.length - errors} imported, ${errors} errors`)
