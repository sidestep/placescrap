import { countries, bookingGet, bookingQuery, load, save, diff, id, hotelUrl, toDateStr, inOutDates, isRoom, addDays, truncUrl, sleep } from 'utils'
import { query as qcalendar } from '../query/calendar.js'
import { query as qrooms } from '../query/rooms.js'
import { toCat, toInOut } from 'wifi-bed/serv/mod/inputs.js'
import { CookieJar } from 'npm:tough-cookie'

const cc = Deno.args[0]
const root = `/content/drive/My Drive`
const path = (hid) => `${root}/${cc}/${hid}.json`
const pause = Deno.args[1] || 1
const { currency } = countries[cc]  
if(!currency)
{
    console.error(`can't find country by ${cc}`)
    Deno.exit(1)
}

const getFacilities = (data) => 
{
    const all = Object.keys(data).filter(k => k.startsWith('BaseFacility')).map(k => data[k])
    const byGroup = Object.groupBy(all, f => f.groupId)
    return Object.entries(byGroup).reduce((acc, [gid, f]) => { acc[gid] = f.map(f => f.instances[0].title); return acc; }, {})
}

const roomPrice = ({b_price_breakdown_simplified: {b_headline_price_amount: price, b_excluded_charges_amount: hiddenPrice}}) => Math.round(price + hiddenPrice)
function paxPrices(room)
{
    const byPax = Object.groupBy(room.b_blocks, r => r.b_max_persons)
    const paxRates = Object.entries(byPax).map(([k,v]) => [parseInt(k), Math.min(...v.map(roomPrice))])
    if(paxRates.length == 1)
        return paxRates[0]
    paxRates.sort()
    return [paxRates.at(0), paxRates.at(-1)].flat()
}

function parseRates(html)
{
    let rooms = /b_rooms_available_and_soldout: (\[.+\]),/.exec(html)
    if(!rooms)
        throw 'no rates'
    rooms = eval(`(${rooms[1]})`)
    if(!rooms.length)
        throw 'no rates found'
    return rooms.map(r => ([r.b_id, paxPrices(r)]))
}

const expandNr = (fnr) => fnr.slice(-1) == 'K' ? parseFloat(fnr) * 1000 : parseFloat(fnr)
async function getAvialableDate(hid)
{
    const cin = toDateStr(addDays(3, new Date())) 
    let calendar = await bookingQuery(qcalendar(cc, hid, cin))
    calendar = calendar.data.availabilityCalendar.days
        .filter(d => d.available)
        .map(d => [expandNr(d.avgPriceFormatted), d.checkin])
        .sort()
    return calendar.length ? calendar[0][1] : undefined
}

//const date = await getAvialableDate('hanohus-hotell,9151266')
const bkgWords = /Picnic area|area|facilities|Outdoor furniture|Additional charge|^Fitness$|\s\(within .+\)/i 
const filterDesc = (arr) => arr.map(w => w.replace(bkgWords, '').trim()).filter(w => w)

const findProp = (data, name) => data[Object.keys(data).find(k => k.startsWith(name))] 
function parseHotel([html, rooms])
{
    let data = /<script.*data-capla-store-data="apollo"[^>]*>([\s\S]*?)<\/script>/i
    data = data.exec(html) 
    if(!data)
        return null
    data = eval(`(${data[1]})`)
    //hotel
    const [data1, data2] = [findProp(data, 'BasicPropertyData'), findProp(data, 'Property:')]
    if(!data1 || ! data2)
        return null
    const {name, location} = data1 
    const {reviews, policies, accommodationType: {type}, houseRules: {checkinCheckoutTimes : {checkinTimeRange: cin, checkoutTimeRange: cout}}} = data2 
    //facilities
    const facilities = getFacilities(data)
    const kitchen = facilities['12']   || [] 
    const activities = facilities['2'] || []
    const outdoors = facilities['13']  || []
    const wellness = facilities['21']  || []
    let desc = filterDesc([...outdoors, ...activities, ...wellness])
    if(policies.pets)
       desc.unshift(policies.pets.petsAllowed == 'NO' ? 'No pets' : 'Pets ok')
    desc = desc.join(',')
    //hotel pics
    let pics = /hotelPhotos: (\[[\s\S]+?\]),/i
    pics = eval(`(${pics.exec(html)[1]})`)
    const img = truncUrl(pics.shift().large_url)
    pics = pics.filter(p => !p.associated_rooms).map(p => truncUrl(p.large_url))
    //rooms
    const rates = new Map(parseRates(html))
    rooms = rooms.data.roomDetail.property.rooms
    rooms = rooms.filter(r => isRoom(r.translations.name))
    const hotel = 
        {
            name, 
            loc: `${location.latitude},${location.longitude}`, 
            type,
            rating: reviews.questions.find(r => r.name == 'total')?.score,
            adr: location.formattedAddress,
            inout: toInOut(cin.fromFormatted, cin.untilFormatted, cout.fromFormatted, cout.untilFormatted),
            cat: toCat({kitchen: kitchen.some(i => ['Kitchen','Shared kitchen'].includes(i))}),
            desc: desc || undefined,
            rooms: rooms.map(r => 
                [r.id, 
                {
                    name: r.translations.name, 
                    rate: rates.get(r.id)?.join(','),
                    pax: r.occupancy.maxPersons,
                    cat: toCat({bathroom: r.bathroomCount > 0}),
                    rms: r.apartmentRooms.length || undefined,
                    m2: r.roomSizeM2 || undefined,
                    pics: r.roomPhotos.map(p => truncUrl(p.photoUri))
                }]),
            img,
            pics
        }
    return hotel
}

async function getHotel(hid)
{
    await sleep(pause)
    const din = await getAvialableDate(hid)
    if(!din)
        throw 'unavailable'
    const [cin, cout] = inOutDates(new Date(din)) 
    const html = bookingGet(`${hotelUrl(cc, hid)}?checkin=${cin}&checkout=${cout}&group_adults=1&group_children=0&no_rooms=1&do_availability_check=0&selected_currency=${currency}`,{cookieJar})
    const rooms = bookingQuery(qrooms(hid))
    return [await html, await rooms]
}
//const hotel = parseHotel(await getHotel('villa-med-9-rum-mojlighet-for-15-sangar,10817811'))
//const h = await getHotel('havsnara-nybyggd-modern-villa-uthyres-aret-runt-monsteras,8434960')
//const hotel = parseHotel(h)
//const html = await bookingget(hotelurl('se', 'nara-metro-skogen-och-stranden'))
//const html = await bookingget(hotelurl('se', 'davids-home'))
//const html = await bookingget(hotelurl('se', 'blique-by-nobis'))
//const html = await bookingGet(hotelUrl('se', 'hamilton-old-town-hostel'))

const lsIds = (dir) => Array.from(Deno.readDirSync(dir)).map(f => f.name.replace('.json', ''))
const batchLen = 10 
function* batched(coll, cnt=batchLen) { for(let idx=0; idx<coll.length; idx+=cnt) yield coll.slice(idx, idx+cnt) }
const allIds = load(`${root}/${cc}-ids.json`)
const haveIds = lsIds(`${root}/${cc}`)
const newIds = diff(allIds, haveIds)

console.log(`[~] ${newIds.length} out of ${allIds.length} hotels to get for ${cc}(${currency}), ${1 + newIds.length/batchLen >>0} batches`)
console.log(`[~] pause set ${pause}`)

const utf8en = new TextEncoder()
let bcnt = 0
const errors = []
const cookieJar = new CookieJar(); 
await bookingGet(`${hotelUrl(cc, allIds.at(-1))}?selected_currency=${currency}`, {cookieJar})// set currency in session
for(const batch of batched(newIds, batchLen))
{
    Deno.stdout.writeSync(utf8en.encode(`${++bcnt}\r`))
    const results = await Promise.allSettled(batch.map(getHotel))
    results.forEach((r, i) => 
    {
        const hid = batch[i]
        try
        {
            if(r.status == "rejected")
                throw r.reason
            const hotel = parseHotel(r.value)
            if(!hotel)
                throw 'no hotel'
            save(path(hid), hotel)
        }
        catch(e)
        {
            errors.push(hid)
            console.error(['---', e, hid, hotelUrl(cc, hid)].join('\n'))
        }
    }) 
   await sleep(2)
}

console.log(`[~] Done, got ${newIds.length - errors.length} new hotels, ${errors.length} errors`)
