export const query = (cc, hid, from) => 
{
    hid = hid.split(',')[0]
    return ({
      "operationName": "AvailabilityCalendar",
      "variables": {
        "input": {
          "travelPurpose": 2,
          "pagenameDetails": {
            "countryCode": cc,
            "pagename": hid
          },
          "searchConfig": {
            "searchConfigDate": { "startDate": from, "amountOfDays": 62 },
            "nbAdults": 1,
            "nbRooms": 1,
            "nbChildren": 0,
            "childrenAges": []
          }
        }
      },
      "extensions": {},
      "query": "query AvailabilityCalendar($input: AvailabilityCalendarQueryInput!) {\n  availabilityCalendar(input: $input) {\n    ... on AvailabilityCalendarQueryResult {\n      hotelId\n      days {\n        available\n        avgPriceFormatted\n        checkin\n        minLengthOfStay\n        __typename\n      }\n      __typename\n    }\n    ... on AvailabilityCalendarQueryError {\n      message\n      __typename\n    }\n    __typename\n  }\n}\n"
    })
}
