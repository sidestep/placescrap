export const query = (hid) =>
{ 
    hid = parseInt(hid.split(',')[1])
    return ({
      "operationName": "RoomPage",
      "variables": {
        "rdsInput": {
          "hotelId": hid,
          "searchConfig": {
            "nbRooms": 1,
            "nbAdults": 1,
            "nbChildren": 0,
            "childrenAges": []
          },
          "highlightedBlocks": [],
          "selectedFilters": "",
          "travelReason": "UNKNOWN"
        },
        "reviewInput": {
          "hotelId": hid, 
          "questions": ["bed_comfort"],
          "customerType": ["ALL"]
        }
      },
      "extensions": {},
      "query": "query RoomPage($rdsInput: RDSRoomDetailQueryInput!, $reviewInput: ReviewScoresInput) {\n  roomDetail(roomDetailQueryInput: $rdsInput) {\n    areaMeasurementUnit\n    property {\n      id\n      name\n      location {\n        cityTranslation {\n          nameIn\n          name\n          __typename\n        }\n        __typename\n      }\n      accommodationType {\n        id\n        type\n        __typename\n      }\n      hasDesignatedSmokingArea\n      rooms {\n        id\n        translations {\n          name\n          description\n          __typename\n        }\n        isBiggerThanAverageRoomInUfi\n        roomTypeId\n        roomSizeM2\n        privacyLevel\n        roomPhotos {\n          id\n          photoUri\n          thumbnailUri\n          __typename\n        }\n        bedConfigurations {\n          configurationId\n          beds {\n            bedType\n            count\n            translation {\n              name\n              __typename\n            }\n            __typename\n          }\n          __typename\n        }\n        occupancy {\n          maxChildren\n          maxGuests\n          maxPersons\n          __typename\n        }\n        apartmentRooms {\n          count\n          id\n          roomType\n          beds {\n            bedType\n            count\n            translation {\n              name\n              __typename\n            }\n            __typename\n          }\n          maxPersons\n          ensuiteBathroom\n          __typename\n        }\n        isSmoking\n        bathroomCount\n        bathroomFacilityAttributes {\n          isEnsuiteBathroom\n          isExternalBathroom\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    hasRoomsWithDifferentPrivacyLevels\n    highlightsForAllRooms {\n      roomId\n      roomHighlights {\n        ... on RDSRoomPrivacyHighlight {\n          privacyLevel\n          __typename\n        }\n        ... on RDSRoomSizeHighlight {\n          areaValue\n          __typename\n        }\n        ... on RDSRoomFacilityHighlight {\n          id\n          translationOverride\n          name\n          __typename\n        }\n        ... on RDSHotelFacilityHighlight {\n          id\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    categorizedFacilitiesForAllRooms {\n      roomId\n      categorizedFacilities {\n        category\n        facilities {\n          id\n          name\n          __typename\n        }\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n  reviewScores(input: $reviewInput) {\n    ... on ReviewScoresResult {\n      reviewScores {\n        name\n        translation\n        value\n        count\n        customerType\n        __typename\n      }\n      __typename\n    }\n    __typename\n  }\n}\n"
    })
}
