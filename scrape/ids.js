import { countries, diff, load, save, path, inOutDates, bookingQuery } from 'utils';
import { query as qsearch } from './query/search.js'
import { addDays } from "utils";

const cc = Deno.args[0] || 'ro'
const indays = parseInt(Deno.args[1] || 5)
const priceFrom = parseInt(Deno.args[2] || 3)
const priceTo = 200

const { name: country } = countries[cc]
if(!country)
{
    console.error(`can't find country by ${cc}`)
    Deno.exit(1)
}

const [checkin, checkout] = inOutDates(addDays(indays))

console.info(`[~] getting ${country} (${cc}) for ${checkin}`)

const priceStep = 1 
let newCnt = 0

const getData = ({data:{searchQueries: {search: data}}}) => data
async function getNewIds(priceFrom, priceTo, hotelIds)
{
    for(let priceA = priceFrom; priceA < priceTo; ++priceA)
    {
        const priceB = priceA + priceStep 
        let resp = getData(await bookingQuery(qsearch(country, checkin, checkout, priceA, priceB, 0)))
        const hotelCnt = resp.pagination.nbResultsTotal 
        let pageCnt =  hotelCnt / 100 >> 0;
        console.log(`${priceA}-${priceB} eur (${hotelCnt} hotels)`)
        if(hotelCnt == 0)
            continue;
        while(pageCnt >= 0)
        {
            const ids = resp.results.map(({basicPropertyData: {pageName, id}}) => `${pageName},${id}`)
            const newIds = diff(ids, hotelIds)
            console.log(` page ${pageCnt} (${newIds.length} new out of ${ids.length})`)
            if(newIds.length > 0)
            {
                hotelIds.push(...newIds)
                save(path(cc, 'ids'), hotelIds) 
                newCnt += newIds.length
            }
            --pageCnt;
            try
            {
                resp = getData(await bookingQuery(qsearch(country, checkin, checkout, priceA, priceB, pageCnt)))
            }
            catch(err)
            {
                console.error(err); 
                ++pageCnt;
            }
        }
    }
}

const hotelIds = load(path(cc, 'ids'))
await getNewIds(priceFrom, priceTo, hotelIds)
console.log(`[~] done: ${newCnt} new, ${hotelIds.length} total`)
