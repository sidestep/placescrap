#!/bin/sh

(until $1; do
    echo "'/bin/myprocess' crashed with exit code $?. Restarting..." >&2
    sleep 1
done)
