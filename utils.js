import got from 'npm:got';
import { Arrays } from "https://deno.land/x/arrays@v1.0.21/mod.ts";

export const countries =
{
    fr: {name: "France", currency: "EUR"},  // 100 million tourists per year
    es: {name: "Spain",  currency: "EUR"},  // 85
    it: {name: "Italy",  currency: "EUR"},  // 57
    tr: {name: "Turkey", currency: "TRY"}, // 55
    gb: {name: "United Kingdom", currency: "GBP"}, // 37
    de: {name: "Germany", currency: "EUR"},     // 34
    gr: {name: "Greece",  currency: "EUR"},    // 32
    at: {name: "Austria", currency: "EUR"},    // 23
    ua: {name: "Ukraine", currency: "UAH"},     // 21
    pt: {name: "Portugal", currency: "EUR"},    // 8
    nl: {name: "Netherlands", currency: "EUR"},
    pl: {name: "Poland", currency: "PLN"},
    ro: {name: "Romania", currency: "RON"},
    se: {name: "Sweden", currency: "SEK"},
    be: {name: "Belgium", currency: "EUR"},
    cz: {name: "Czech Republic", currency: "CZK"},
    hu: {name: "Hungary", currency: "HUF"},
    hr: {name: 'Croatia', currency: "HRK"},     // 9
    ch: {name: 'Switzerland', currency: "CHF"}, // 8
    dk: {name: 'Denmark', currency: "DKK"},     // 7
    fi: {name: 'Finland', currency: "EUR"},     // 6
    no: {name: 'Norway', currency: "NOK"},      // 6
    ie: {name: 'Ireland', currency: "EUR"},     // 5
    bg: {name: 'Bulgaria', currency: "LEV"},    // 4
    lv: {name: 'Latvia', currency: "EUR"},      // 3
    ee: {name: 'Estonia', currency: "EUR"},     // 3
    lt: {name: 'Lithuania', currency: "EUR"},   // 3
    si: {name: 'Slovenia', currency: "EUR"},    // 3
    sk: {name: 'Slovakia', currency: "EUR"},    // 2
    al: {name: 'Albania', currency: "LEK"},     // 2
    cy: {name: 'Cyprus', currency: "EUR"},      // 2
    mt: {name: 'Malta', currency: "EUR"},       // 1
}

const write = Deno.writeTextFileSync
const read = Deno.readTextFileSync

const readSafe = (path) => {try { return read(path) } catch(err) { if(err.name == "NotFound") return null; throw err; }}
export const path = (cc, posfix='') => new URL(`data/${cc}${posfix ? '-'+posfix : ''}.json`, import.meta.url).pathname
const parseSafe = (path) => { const json = readSafe(path); return json ? JSON.parse(json) : [] } 
const toLineStr = (arr) => `[${arr.map(a => JSON.stringify(a)).join('\n,')}]\n` 
const format = (obj) =>
    Array.isArray(obj) ? toLineStr(obj)
        : obj instanceof Map ? toLineStr(Array.from(obj)) 
        : obj instanceof Object ? JSON.stringify(obj, null, 2)
        : obj.toString() 
export const load = (path) => parseSafe(path) 
export const save = (path, obj) => write(path, format(obj)) 

const headers = 
{
    "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
    "Host": "www.booking.com"
}
export const bookingGet = (url, params={}) => got.get(url, {headers, ...params}).text()
export const bookingQuery = (q, curr='EUR', params={}) => got.post(`https://www.booking.com/dml/graphql?lang=en-gb&selected_currency=${curr}`, {json:q, headers, ...params}).json()

export const diff = (arr1, arr2) => Array.from(arr1).diff(Array.from(arr2))
export const hotelUrl = (cc, id) => `https://www.booking.com/hotel/${cc}/${id.split(',')[0]}.html`
export const picUrl = (path) => `https://cf2.bstatic.com/xdata/images/hotel/max640/${path}`
export const truncUrl = (url) => url.slice(url.lastIndexOf('/')+1)
export const id = (p) => p[0]
export const picTag = (p) => p[1][0]
export const tagScore = (p) => p[1][1]

export function loadAll(cc)
{
    const hotels = new Map(load(path(cc, 'hotels')))
    const pics = new Map(load(path(cc, 'pics')).flat())
    const tagged = []
    for(const [hid, h] of hotels)
    {
        if(!pics.has(hid))
            continue
        h.pics = pics.get(hid)
        h.rooms.forEach(([rid, r]) => r.pics = pics.get(rid))
        h.rooms = removeDuplicates(h.rooms)
        tagged.push([hid, h])
    }
    return tagged
}

export function removeDuplicates(rooms)
{
    const byNameRate = Object.groupBy(rooms, r => `${r.name} ${r.rate || crypto.randomUUID()}`)
    return Object.values(byNameRate).map(offers => offers.sort((o1,o2) => o1.pics.length > o2.pics.length ? -1 : 1)[0])
}

export const toDateStr = (date) => date.toISOString().split('T')[0]
export const addDays = (days, date = new Date()) => { date.setDate(date.getDate() + days); return date }
export const inOutDates = (cin) => [toDateStr(cin), toDateStr(addDays(1, cin))]

export const nonRooms = ['Bed in', 'Single Bed', 'Bunk Bed', 'Tent', 'Empty lot', 'Dormitory Room', 'Shared Room'] 
export const isRoom = (name) => !nonRooms.some(s => name.includes(s))

export const spawn = (cmd) => new Deno.Command(cmd).spawn()
export const sleep = (s) => new Promise(resolve => setTimeout(resolve, s * 1000));
export function run(cmd)
{ 
    if(typeof cmd === 'string')
        cmd = cmd.split(/\s+/);
    const { code, stdout, stderr } = new Deno.Command(cmd.shift(), {args: cmd}).outputSync() 
    const toTxt = new TextDecoder()
    if(code)
        throw `${cmd}: ${toTxt.decode(stderr)}`
    return toTxt.decode(stdout)
}

import { expandGlobSync } from "https://deno.land/std@0.66.0/fs/mod.ts";
export const glob = (g) => expandGlobSync(g).map(p => p.path)
export const mkdir = (d) => Deno.mkdir(d, {recursive: true})
//const cleanWords = (path) => { const hotels = load(path); hotels.forEach(([_,h]) => {if(h.desc) h.desc = removeBkgWords(h.desc)}); save(path, hotels); } 
//const p = hotelsPath('se') 
//console.log('cleaning', p)
//cleanWords(p)
//console.log('done')
