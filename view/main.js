import { view } from './index.html.js'
import { hotelUrl, loadAll, picUrl, run } from 'utils'
import { setPics } from '../import/pics.js'

const write = Deno.writeTextFileSync

const cc = 'se'
const outFile = `./${cc}.html`

let hotels = loadAll(cc)
//let hotels = loadAll(cc).filter(([hid,h]) => hid == 'selholmens-camping')
const expandUrl = (pics) => pics.map(([p, tag]) => [picUrl(p), tag])
const expandPics = (b, allPics) =>{b.pics = expandUrl(b.pics); b.allPics = expandUrl(allPics)} 

hotels = hotels.map(([hid, h]) =>
    {
        const [placePics, roomPics] = [h.pics, h.rooms.map(r => r.pics)] 
        setPics(h) 
        expandPics(h, placePics)
        h.rooms.forEach((r,i) => expandPics(r, roomPics[i]))
        return [hotelUrl(cc, hid), h]
    })

write(outFile, view(hotels))

//const showErrors = (kind) => {const e = err.filter(e => e.startsWith(kind)); if(e.length) console.error(e.join('\n')); return e;}
console.log(`[~] done, ${hotels.length} extracted`)
run(['firefox', outFile])
console.log(hotelUrl(cc, ''))
