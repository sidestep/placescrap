const page = (hotels) =>
`<!DOCTYPE html>
<html lang='en'>
<head>
<style>
    .hotel {display: flex; flex-direction: column;}
    figure {padding:0; margin:5px;} 
    .pics{display: flex; flex-direction: row;} 
    figure img {width:250px; height:200px; object-fit: contain;}
    .selected {background-color: gainsboro;}
</style>
</head>
<body>
    ${hotels}
</body>`

const pic = ([url, tag]) => 
`<figure>
    <img src='${url}' loading="lazy"/>
    <figcaption>
        <a href='${url}' target='_blank'>${tag}</a>
    </figcaption>
</figure>`

const block = ({name, pics, rate='', allPics}) => 
`<div class='block'>
    <h3 class='name'>${name} ${rate}</h3>
    <div class='pics'>
        ${allPics.map(pic).join('\n')}
    </div>
    <div class='pics selected'>
        ${pics.map(pic).join('\n')}
    </div>
</div>`

const hotel = ([url, h]) => 
{
    try
    {
        return `<div class='hotel'>
                <a target='_blank' href='${url}'>
                <h2>${h.name}</h2>
                <div>${h.adr}</div>
                ${pic(h.pics[0])}
            </a>
            <div>${h.cat == 2 ? 'Has Kitchen! ' : ' '} ${h.desc}</div>
            ${[block(h), ...h.rooms.map(r => block(r))].join('\n')}
        </div>
        <hr/>`
    }
    catch(e)
    {
        console.log(e, url)
    }
}

export const view = (hotels) => page(hotels.map(hotel).join('\n')) 
