import { load, save, hotelsPath } from '../scrape/utils.js'

const cc = 'se'
const path = hotelsPath(cc)
let tagged = load(path)
const toArrTags = (tags) => tags.map(stag => JSON.parse(stag.replaceAll("'", '"')))
tagged.forEach(([hid, h]) => 
    {
        if(h.tagged)
        {
            try
            {
                h.tags = toArrTags(h.tags) 
                h.rooms.forEach(([_, r]) => r.tags = toArrTags(r.tags))
            }
            catch(e)
            {
                console.error(hid, e)
            }
        }
    })

save(path, tagged)
