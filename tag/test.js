import { tag } from './tag.js'
import { run } from 'utils'

let pics = [
"https://cf2.bstatic.com/xdata/images/hotel/max640/329529270.jpg?k=5b54884264629791ba8df15e0859dd566d773f29b4f2c00fa8b503a13ec006bb&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/329529264.jpg?k=99512d9e1366df0fad728393a4998c6c5bbfc9c735a526ab6582bd49e26539ab&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/89976665.jpg?k=860c8c5eae4c36b1f6affa2a7e18a6d28fa39c01d13558000b35b3241af2c49b&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/287743008.jpg?k=8fc4647dbd883896c9e9117cf24d3440ea3a3d4bccad52d839268a58efa5cc60&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/89976675.jpg?k=db3f45c7e33048e6f8d4f2f561a02c46511edaa4477bbc41389a19b6c2831c75&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/233954505.jpg?k=ffd3df3c4387262eb286528eb2b33406861401eba75173b486d2e2d51cd856dd&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/585925042.jpg?k=ab0bfc29ab7c7656d7e0caa0a11b8a899e35a83f538d211655c8c7cac34af45c&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/585925054.jpg?k=fdc3d21cc9e0969f90d08acf54911eb996344119c2eccc7220e26bf2d0fa0674&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/494261582.jpg?k=a45166bcc28b8e4b41e8d6e682d9c3035effedea62d9ef6d084dcdc2dbfb4302&o=",
"https://cf.bstatic.com/xdata/images/hotel/max1024x768/605795955.jpg?k=47d5c07338545c7080f8ab49e3407ff9b0d71a54ef6d4a54fe34f5ce1314abe0&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/161597714.jpg?k=010d600c76000264347b4d9d210f53908afaa056fd28eb602194b2baae75fcfb&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max640/246773733.jpg?k=67a3e62baff610ea7af061b3fb0d642a46c3cecdbb413f45b7ad30df2d714a89&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max640/477574971.jpg?k=927904da50c8e9f4a5528cd2528a9c1db0fa2a6f9d40d17b34823b0c0702193f&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/463354752.jpg?k=e2dfca0df111a70af6f44295a61bcb82e18b9c47e4bae0def29f60e246934b1f&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/476582641.jpg?k=f6b854e2f81a5193fabef4fd7cbbdd186c65b4d21e27bae0b884dbad28fc1abb&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max640/465930829.jpg?k=80e017433b097d238a224faa3515f29338f53f459438a1b3d6841ec6f13b9926&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/463365444.jpg?k=196613085d1dc0b02b555ef9cd64ae62de5105a9ffdb2631d89f9ded8f810557&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/463365445.jpg?k=2022b4337fd70f2cf27bc034b956f3197b050aa0d5ad727185686487305f7264&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/527349331.jpg?k=c30703b5704a3f8c90a125fc5d9726b7657448d4fb616ef581019b9fa154c5a9&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/527349346.jpg?k=706037d3136d2a1c6f11812b675bb3a143e91a9271754ab346b5a6c81553b3d1&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/527347750.jpg?k=619ad0fdf4c7e74a51da72be642560e54ff3421e0a30655e074a4ce4f326455d&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/527347748.jpg?k=7792cb00873eac5fb68cd2d4486f1357b69d024e26b3ba5fca983a3d6a993945&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/372833439.jpg?k=055c7690650964f5847ed8382ddfbbc79e062b3aa2747360eb225243aedd5a09&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/372833434.jpg?k=c493d2c5066f85d956df0569d0cce68d537f87bf9a1d88ec7afe531b057892c6&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/269520916.jpg?k=32ed2324580ac1254f462962bcf310c8418bd3bb2393dd04ce66d506feb35222&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/285539004.jpg?k=6707706c6ab8a06fe2efa3fb39213aab44d8e590f1471879789ae2672c647bf3&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/576801543.jpg?k=943951463d686f4084c447760bb54230f7376bc8ef8ff3560099106f650c73d2&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/576801545.jpg?k=2a252394a72144113a6e78b469b8aa78849dda67acacb9ce5830e49565ca2e80&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/452035670.jpg?k=887638915239fb758a70ef1467f6a886f8fe5e427239b97e4647483445e75585&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/343094561.jpg?k=3db30d187fabe9a7050fcf0951b23ebebea7dcbbf1d3214add0d170ccfb7175d&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/372364834.jpg?k=ed0aef81e900f55bc82d9250bb3038c0992a161c5c13b39bacbf049954e5a2cb&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/161597724.jpg?k=25cc821e41357198fec8ccda0b9b6a10bf97b8f3e20477937f0b148ba05b5a44&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max640/161594393.jpg?k=fac097c27ba43a9904932e9266bd7faed5253452b31726f92cf6999facdf430e&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max640/161597735.jpg?k=23c2d56fdea7cadc3403525abe5c6a34b8ee61fda972d0d2534e74c620e17c91&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max640/161594033.jpg?k=72121120c217785982622ca54d8104a63a335acf4c324edaa2aa9858ac42ab64&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max640/161597721.jpg?k=302bc279c3a174b1e4fe7ab2a69acf392cc644303d3bd55cea443add0d541342&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max640/366857502.jpg?k=44d2c8f8da12f5d10c61499a62fc32a9c0ee8c9c4ca03669e4873a64c1690ec0&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max640/352879868.jpg?k=8380a3445489d0efd4127371370721f9c8acb23762a753cc87523ac56b6f682a&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max640/372314699.jpg?k=bed01425d21237bb61bcddfaab57cb47b70c204e44e4fe281de00214fd2ca22c&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/330371678.jpg?k=a6caefa02223c11766f7519234bc27b00131abd516eb03ff8242e49bd3a2f800&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/329531024.jpg?k=dc4567e6629b7d757511bc5cab344e9471c4436f9c1c05c2e73d578b14e6c450&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/199589541.jpg?k=c013fade2c181b69ee211a0c9edc00634ddf943d4bca48e7c99145a2cf26a0a4&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/523342787.jpg?k=829029f6ed498ea4040ce0b63982d163e340319801b2d9e6da9f89305ecad202&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/523353054.jpg?k=7a03e482981a1c315c5d5669218c64d414550272295bfd35655c30aa1a951445&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/523353906.jpg?k=a4480aac5995e717f5ceee8954b05587f097faf0894f4f045f0de81491b52b11&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/523342790.jpg?k=96a0b80696813fad6dc171bb6619d5fcff7b1a45a577a333560b206b5d12f44f&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/465283630.jpg?k=a2a1932ecc4f70e4ff04942e96d893f33a61dbefb48f773ced59502bc3641d3a&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/576801547.jpg?k=635752d569e2cad7d33c6b9e031f82dd1913d542709c6b9ecea28255fab19dfc&o=",
"https://cf2.bstatic.com/xdata/images/hotel/max640/463354714.jpg?k=f115146256b0227f8f7e0af32f1736a2e567e054e5ef03e2b51bb3ddb612152a&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max640/463354704.jpg?k=99bcd9a2d749d9330b555466df68fb9245e2ec80c11ae84583d1a392a7b93f7e&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291840.jpg?k=e8215feab9322ec385ef7b4c4b90b0b17da9702699b917c6c2b3901bb5776302&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291932.jpg?k=c96ca1a313ad7385cf1bda0317d8e57296643cf0cd113f9e2642018ec75cf82d&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291978.jpg?k=f4ebc2776df54a15b5793a81c53ddc5d4acc1595d685b4d3b13b7715190b6f5c&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291986.jpg?k=e2586c42ece93591a87c6affe011d67a767ecd41d737bb5e41615179812454b0&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291955.jpg?k=c8ea4dcf8777cf05d50b7cde19d51d6b05108fab8e4e07d56ad7fde334fbfdcb&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291962.jpg?k=3d843a4ee3240ab6cc8c052c00a0913ec35e5f6740975e39230d725a34587141&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291947.jpg?k=793fb3d2972a45242a9b4e9806ad83dfc3ed87580acf21f94091ff118f06346a&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291912.jpg?k=d8f4b5d41def02b8ea08b7fb29b99388d348c69ff67161f4a47b29a24358b597&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291924.jpg?k=dccd933a8cb30c45d3a5bbf6350431bd2ed9f3e0937317dcabc181b9d519df54&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291942.jpg?k=bb952604130d1621d334d72b13d4b1d8347670d57f3c062170c1a6836960861f&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291971.jpg?k=74e08e13975cc3bb7944916712ac228bce61abc1fb7d14520c6dd327369dc57d&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/558794862.jpg?k=620a6f56ff8baf1f2acd92bce4aa16f38724ae62488c62fea51d5a6b303101f1&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/558795279.jpg?k=8680b0a158f9d633add952d028ea3e34a7492bbd82e8038efd18d31732eafcff&o=&hp=1",
"https://cf2.bstatic.com/xdata/images/hotel/max640/492025537.jpg?k=7700d5a9946ce494e0108020ae5b5f64c1874fd23b1fc4e7966c48b1898d6153&o=&hp=1",
]

console.log(`[~] tagging ${pics.length} pics`)
const tags = await tag(pics)
pics = pics.map((p,i) => [p, tags[i]])

const tagPic = ([pic, tag]) => `<figure><a href='${pic}' target='_blanc'><img src='${pic}'/></a><figcaption>${tag}</figcaption></figure>`
const page = (pics) =>
`<!DOCTYPE html>
<html lang='en'>
<head>
<style>
    body {display: flex; flex-direction: row; flex-wrap: wrap;}
    figure {padding:0; margin:5px;} 
    figure img {width:350px; height:250px; object-fit: contain;}
    figcaption {font-weight: bold;}
</style>
</head>
<body>
    ${pics.map(tagPic).join('\n')}
</body>`
console.log('[~] done')

Deno.writeTextFileSync('/tmp/tagtest.html', page(pics))
run(['firefox', '/tmp/tagtest.html'])
