from PIL import Image
import requests
from transformers import CLIPProcessor, CLIPModel
import json
import os
from google.colab import drive
import torch

cc = 'it'

device = 'cpu'
if torch.cuda.is_available():
  device = 'cuda'
  print('gpu available', torch.cuda.get_device_name(0))

drive.mount('/content/drive')
os.makedirs(f'/content/drive/My Drive/{cc}', exist_ok=True)
outFile = lambda fid: f'/content/drive/My Drive/{cc}/{fid}.json'
inFile = f'/content/drive/My Drive/{cc}.json'
with open(inFile, 'r') as f:
    in_pics = json.loads(f.read())

tags = ["kitchen", "bathroom", 'toilet with shower', 'bedroom', 'bedroom with bed and furniture', 'livingroom with furniture', 'outdoors', 'house', 'furniture', 'logo', 'reception', 'hallway']

model = CLIPModel.from_pretrained("openai/clip-vit-base-patch32").to(device)
processor = CLIPProcessor.from_pretrained("openai/clip-vit-base-patch32")

def load_pics(urls):
    picids = []
    images = []
    for url in urls:
        try:
           img = Image.open(requests.get(url, stream=True).raw)
        except Exception as e:
            print('cant load', url, e)
            continue
        picids.append(url)
        images.append(img)
    return (picids, images)

def tag(pics):
    urls, images = load_pics(pics)
    if not images:
        return []
    inputs = processor(text=tags, images=images, return_tensors="pt", padding=True).to(device)
    outputs = model(**inputs).logits_per_image
    probs = outputs.softmax(dim=1) # we can take the softmax to get the label probabilities
    max_probs, max_idxs = probs.max(dim=1)
    tagged = [[urls[i], [tags[max_idxs[i].item()], max_probs[i].item()]] for i in range(len(urls))]
    return tagged

def picUrl(path):
  return f'https://cf2.bstatic.com/xdata/images/hotel/max640/{path}'

def truncUrl(url):
   return url[url.rindex('/')+1:]

cnt = 0
for pics in in_pics:
    hid = pics[0][0]
    if os.path.exists(outFile(hid)):
        continue
    print(++cnt, end="\r", flush=True)
    for kpics in pics:
        pic_urls = [picUrl(pid) for pid in kpics[1]]
        tagged = tag(pic_urls)
        trimmed = [[truncUrl(purl), tag] for purl,tag in tagged]
        kpics[1] = trimmed
    with open(outFile(hid), 'w+') as f:
        f.write(json.dumps(pics))
