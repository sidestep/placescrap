import { load, save, path, hotelUrl, picUrl, truncUrl, id, diff } from "utils";
import { tag } from './tag.js'

const cc = Deno.args[0] || 'ua'

async function tagPics(pics)
{
    if(pics.length == 0)
        return []
    const tagged = await tag(pics.map(picUrl))
    tagged.forEach(tp => tp[0] = truncUrl(tp[0]))
    return tagged
}

const hotels = new Map(load(path(cc, 'hotels')))
let pics = new Map(load(path(cc, 'pics')))

const hasDiff = (a,b) => a.sort().toString() != b.sort().toString()
const changed = (hotel, [ppics, rpics]) => 
    hasDiff(hotel.rooms.map(id), rpics.map(id)) || 
        hasDiff(hotel.pics, ppics.map(id)) || 
        rpics.some(([_,pcs],i) => hasDiff(hotel.rooms[i][1].pics, pcs.map(id)))

//console.log(changed(hotels.get('quality-friends,546866'), pics.get('quality-friends,546866'))); //!--- delete me!

const needsRetag = new Set()
pics.forEach((hid, pics) =>
{
    if(!hotels.has(hid))
        return
    if(changed(hotels.get(hid), pics))
        needsRetag.add(hid) 
})
console.log(`[~] retagging ${needsRetag.size} hotels`)
if(needsRetag.size)
{
    pics = pics.filter(([hid,_]) => !needsRetag.has(hid))
    save(path(cc, 'pics'), pics)
}

const untagged = diff(hotels.keys(), pics.keys())
console.log(`[~] tagging ${untagged.length} out of ${hotels.size} hotels`)
const utf8en = new TextEncoder()
let i = 0
let err = 0
for await (const hid of untagged)
{
    Deno.stdout.writeSync(utf8en.encode(`${i++}\r`))
    try
    {
        const hotel = hotels.get(hid)
        const placePics = await tagPics(hotel.pics)
        const roomPics = await Promise.all(hotel.rooms.map(async ([rid, r]) => [rid, await tagPics(r.pics)]))
        pics.set(hid, [placePics, roomPics])
        save(path(cc, 'pics'), pics)
    }
    catch(e)
    {
        console.error(hotelUrl(cc, hid), e)
        ++err
    }
}

console.log(`[~] Done, ${i-err} hotels tagged, ${err} errors`)

//test 
//const h = allHotels.find(([hid,_]) => hid == 'amarant')    
//tag(h)
