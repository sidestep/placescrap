from transformers import pipeline
import torch
import sys
import json

dev = 'cuda' if torch.cuda.is_available() else 'cpu'

pipe = pipeline("image-classification", model='./models/room_classifier', device=dev)

def tag(pic):
    try:
        labels = pipe(images=pic)
        return [labels[0]['label'], labels[0]['score']]  
    except Exception as e:
        return f'error: ${e}'


#pics = [
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291840.jpg?k=e8215feab9322ec385ef7b4c4b90b0b17da9702699b917c6c2b3901bb5776302&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291932.jpg?k=c96ca1a313ad7385cf1bda0317d8e57296643cf0cd113f9e2642018ec75cf82d&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291978.jpg?k=f4ebc2776df54a15b5793a81c53ddc5d4acc1595d685b4d3b13b7715190b6f5c&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291986.jpg?k=e2586c42ece93591a87c6affe011d67a767ecd41d737bb5e41615179812454b0&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291955.jpg?k=c8ea4dcf8777cf05d50b7cde19d51d6b05108fab8e4e07d56ad7fde334fbfdcb&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291962.jpg?k=3d843a4ee3240ab6cc8c052c00a0913ec35e5f6740975e39230d725a34587141&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291947.jpg?k=793fb3d2972a45242a9b4e9806ad83dfc3ed87580acf21f94091ff118f06346a&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291912.jpg?k=d8f4b5d41def02b8ea08b7fb29b99388d348c69ff67161f4a47b29a24358b597&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291924.jpg?k=dccd933a8cb30c45d3a5bbf6350431bd2ed9f3e0937317dcabc181b9d519df54&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291942.jpg?k=bb952604130d1621d334d72b13d4b1d8347670d57f3c062170c1a6836960861f&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/528291971.jpg?k=74e08e13975cc3bb7944916712ac228bce61abc1fb7d14520c6dd327369dc57d&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/558794862.jpg?k=620a6f56ff8baf1f2acd92bce4aa16f38724ae62488c62fea51d5a6b303101f1&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/558794890.jpg?k=ca75a5d33800f5b333f74a778c0024551f3977e9b52b6ae8348f8102e03c0fc2&o=&hp=1",
#"https://cf2.bstatic.com/xdata/images/hotel/max1024x760/558795279.jpg?k=8680b0a158f9d633add952d028ea3e34a7492bbd82e8038efd18d31732eafcff&o=&hp=1"]

for line in sys.stdin:
    print(json.dumps(tag(line.strip())), flush=True, end='')
