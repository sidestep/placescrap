from PIL import Image
from ultralytics import YOLO
from urllib.request import urlopen
import io
import sys
from collections import Counter
import torch

#pics = ['https://cf2.bstatic.com/xdata/images/hotel/max1024x768/562337799.jpg?k=680512fe362cbb629926931de1829c8a63da6be9bb184e7ada116668b9141ce4&o=&hp=1', 
#        'https://cf2.bstatic.com/xdata/images/hotel/max1024x768/562337800.jpg?k=c6c0c5d72c58f9a3c1779a782be1043621e091754095b8dfc1dafc328731b3e9&o=&hp=1',
#        'https://cf2.bstatic.com/xdata/images/hotel/max1024x768/562318696.jpg?k=a844aeae72b2d4103fabed8f8785d8dcb7e98b5fd41ef0f6be798b389ba6bc19&hp=1']

toBytes = lambda url: Image.open(io.BytesIO(urlopen(url).read())) 
names = lambda result: [r['name'].replace(' ', '_') for r in result.summary()]

model = YOLO("./models/yolov10x.pt")

if torch.cuda.is_available():
    model.to('cuda')

def tag(pic):
    try:
        res = model(source=toBytes(pic), verbose=False)
        return dict(Counter(names(res[0])))
    except Exception as e:
        return f'error: ${e}'

for line in sys.stdin:
    print(tag(line), flush=True, end='')
