from PIL import Image
import requests
from transformers import CLIPProcessor, CLIPModel
import sys
import json
from pathlib import Path
import traceback
import torch

device = 'cuda' if torch.cuda.is_available() else 'cpu'

model_dir = Path(__file__).resolve().parent/'../models/clip'

model = CLIPModel.from_pretrained(model_dir)
processor = CLIPProcessor.from_pretrained(model_dir, clean_up_tokenization_spaces=True)

tags = ["kitchen", "bathroom", 'toilet with shower', 'bedroom', 'bedroom with bed and furniture', 'livingroom with furniture', 'outdoors', 'house', 'furniture', 'logo', 'reception', 'hallway']  

def load_pics(urls):
    picids = []
    images = []
    for url in urls:
        try:
           img = Image.open(requests.get(url, stream=True).raw)
        except Exception as e:
            continue
        picids.append(url)
        images.append(img)
    return (picids, images) 

def tag(pics):
    urls, images = load_pics(pics)
    if not images:
        return []
    inputs = processor(text=tags, images=images, return_tensors="pt", padding=True).to(device)
    outputs = model(**inputs).logits_per_image
    probs = outputs.softmax(dim=1)
    probs, idxs = probs.max(dim=1)
    tagged = [[urls[i], [tags[idxs[i].item()], probs[i].item()]] for i in range(len(urls))]
    return tagged

for line in sys.stdin:
    try:
        pics = json.loads(line.strip())
        print(json.dumps(tag(pics)), flush=True, end='')
    except Exception as e:
        print(f'error: ${traceback.format_exc()}', flush=True, end='')
