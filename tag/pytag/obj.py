from types import TracebackType
from transformers import pipeline
import sys
from collections import Counter

#pics = sys.argv[1:]
#if not pics: exit(1)
#pics = ['https://cf2.bstatic.com/xdata/images/hotel/max640/562337799.jpg?k=680512fe362cbb629926931de1829c8a63da6be9bb184e7ada116668b9141ce4&o=&hp=1', 
#        'https://cf2.bstatic.com/xdata/images/hotel/max640/562337800.jpg?k=c6c0c5d72c58f9a3c1779a782be1043621e091754095b8dfc1dafc328731b3e9&o=&hp=1',
#        'https://cf2.bstatic.com/xdata/images/hotel/max640/562318696.jpg?k=a844aeae72b2d4103fabed8f8785d8dcb7e98b5fd41ef0f6be798b389ba6bc19&hp=1',
#        'https://cf2.bstatic.com/xdata/images/hotel/max640/146964869.jpg?k=c39d1629dc37d18cbe4b15f24f30a40a73514af7fb7c34f4c0ec89b7684e657e&o=',
#        'https://cf2.bstatic.com/xdata/images/hotel/max640/215651098.jpg?k=975b63a9d45b8839283f645b03af0e522dfb75e9717a8983d901dc394d106176&o=',
#        'https://cf2.bstatic.com/xdata/images/hotel/max640/215651107.jpg?k=aaaf5afa3240dd009cb388a1552972d3ae1826c9cde4300c4ff517eff668096a&o=',
#        'https://cf2.bstatic.com/xdata/images/hotel/max640/303221453.jpg?k=72d0712b99275cfa6eab97e6d7f4e67d707d7f1156a49438cda73f0d9e605a15&o=',
#        'https://cf2.bstatic.com/xdata/images/hotel/max640/303221488.jpg?k=f802d2f4e6bad0907a4713dc201b72e4970fa342972a7785a43e293dbc47c53c&o=',
#        'https://cf2.bstatic.com/xdata/images/hotel/max640/117018846.jpg?k=1adebd4bb532b6ca77711dd12d3ffb6014390df28e19c008bc5f83ddf8161137&o=',
#        'https://cf2.bstatic.com/xdata/images/hotel/max640/117018850.jpg?k=862f06c778c8fd8b0dd460a92ec11858a46009975843b2cbfb418e95f2f67b0b&o=',
#        'https://cf2.bstatic.com/xdata/images/hotel/max640/145854663.jpg?k=01414296bdb1b91695f7265a2a5ad32451389f0170df65cf7419ec43027cd90c&o=']

pipe = pipeline("object-detection", model="./models/detr-resnet-50", device='cuda')
names = lambda result: [r['label'].replace(' ', '_') for r in result]

def tag(pic):
    try:
        labels = pipe(inputs=pic)
        return dict(Counter(names(labels)))
    except Exception as e:
        return f'error: ${e}'

for line in sys.stdin:
    print(tag(line.strip()), flush=True, end='')

#print(tag('https://cf2.bstatic.com/xdata/images/hotel/max640/562337799.jpg?k=680512fe362cbb629926931de1829c8a63da6be9bb184e7ada116668b9141ce4&o=&hp=1'))
