const proc = new Deno.Command("python", {args: ['pytag/clip.py'], stdout: "piped", stdin: "piped" }).spawn()
const stdin = proc.stdin.getWriter()
const stdout = proc.stdout.getReader()  

const dec = new TextDecoder()
const enc = new TextEncoder()
export async function tag(pics)
{
    await stdin.write(enc.encode(JSON.stringify(pics) + '\n'))
    let resp = await stdout.read()
    resp = dec.decode(resp.value)
    if(resp.startsWith('error'))
        throw resp
    return JSON.parse(resp)
}
